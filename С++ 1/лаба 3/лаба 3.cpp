// ������������3.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <ctime>
#include <locale.h>



int main()
{
	//if-�������� ��������� ��������
	// if(���_���)
	// {
	//    ���, ����������� ���� (���_���)== true
	//}
	//else
	//{
	//    ���, ����������� � ��������� ������
	//}

	setlocale(LC_ALL, "Russian");
	std::cout << "������������� �����" << "\n" << "\n";

	int a = 10, b = 15, c = 15;
	if (a == b) // == - ��� �������� ���������
				// != - ��� ��������, ����������� �����������
	{
		std::cout << "a==b" << "\n"; // a=b
	}
	else
	{
		std::cout << "a!=b" << "\n"; // a != b
	}

	// ��������� ��������(����������� ������)
	// (���_���������) ? (��������� ���� true) : (��������� ���� false);
	// ������������ ������ ������ \/

	(a == b) ? (std::cout << "a==b") : (std::cout << "a != b");
	std::cout << "\n" << "\n";



	//������ � ��������� �������������           

	int d = 0;
	(d == 0) ? (std::cout << "d=0") : ((d < 0) ? (std::cout << "d<0") : (std::cout << "d>0"));

	std::cout << "\n" << "\n";
	/*


	switch - �������� ��������� �������� � ����������� ����������

	switch(��������� 1)
	case (��������� 2):
	{
	//aaaaaaaa
	}
	case (��������� 3):
	{
	//bbbbbbbbb
	}
	default:
	{
	//ccccccccccccccc
	break;
	}
	*/



	int page_number = 2; //0-���������
						 //1-���������
						 //2- �������
						 //3-���������
	switch (page_number)
	{
	case 0:	// ����� ���������� ���� ������ ��� ������
		std::cout << "start page" << "\n";
		break;
	case 1:
		std::cout << "message page" << "\n";
		break;
	case 2:
		std::cout << "news page" << "\n";
		break;
	case 3:
		std::cout << "settings page" << "\n";
		break;
	default:
		std::cout << "some another page" << "\n";
		break;
	}

	/*
	SWITCH ������������� ��������� �� 7 ���������
	�� ��� �� ������� ��������� ����
	������ 7�� ��������� ����� ����� ������
	*/


	/*
	for-�������� ����� �� ������

	for(�����.���������� ; ������� ����������� ; ����������� ��������)
	{
	���� �����
	}

	*/

	std::cout << "\n";


	/*
	�������� ������: ������ ����� ���������� ������� int i
	������ ������������ ��������
	������ �����������/���������� i++
	*/

	for (int i = 0; i < 10; i++)
	{
		std::cout << "i= " << i << "\n";
	}
	std::cout << "\n";
	for (int i = 0, j = 5;
		i + j < 10;
		i++, j = j + 2, std::cout << "i = " << i << "\t" << "j = " << j << "\n")
	{
		//��������, �����������, ���� i+j<10
	}

	for (int i = 0; i < 10; i++)
	{
		int l = 20;
		std::cout << "i=" << i << "\n";
		std::cout << "l=" << l << "\n";

	}

	

	getchar();
	return 0;
}

