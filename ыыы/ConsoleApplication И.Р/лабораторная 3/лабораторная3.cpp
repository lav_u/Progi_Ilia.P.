// лабораторная3.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <ctime>
#include <locale.h>



int main()
{
	//if-оператор условного перехода
	// if(лог_усл)
	// {
	//    код, выполняемый если (лог_усл)== true
	//}
	//else
	//{
	//    код, выполняемый в противном случае
	//}

	setlocale(LC_ALL, "Russian");
	std::cout << "Кириллический текст" << "\n" << "\n";

	int a=10,b=15,c=15;
	if (a == b) // == - лог оператор сравнения
			  // != - лог оператор, проверяющий неравенство
	{
		std::cout << "a==b" << "\n"; // a=b
	}
	else
	{
		std::cout << "a!=b" << "\n"; // a != b
	}
	
	// тернарный оператор(сокращенная запись)
	// (лог_выражение) ? (операторы если true) : (операторы если false);
	// переписываем первый пример \/

	(a == b) ? (std::cout << "a==b") : (std::cout << "a != b");
	std::cout << "\n"<<"\n";



	//ПРИМЕР С ПРОВЕРКОЙ ДИСКРИМИНАНТА           

	int d = 0;
	(d == 0) ? (std::cout << "d=0"): ((d < 0) ? (std::cout << "d<0") : (std :: cout << "d>0"));

	std::cout<<"\n"<<"\n";
	/*
	

	switch - оператор условного перехода с несколькими выриантами

	switch(выражение 1)
		case (выражение 2):
		{
			//aaaaaaaa
		}
		case (выражение 3):
		{
			//bbbbbbbbb
		}
		default:
		{
			//ccccccccccccccc
			break;
		}
	*/



	int page_number = 2; //0-стартовая
						 //1-сообщения
						 //2- новости
						 //3-настройки
	switch (page_number)
	{
	case 0:	// блоки операторов можо писать без скобок
		std::cout << "start page" << "\n";
		break;
	case 1:
		std::cout << "message page" << "\n";
		break;
	case 2:
		std::cout << "news page" << "\n";
		break;
	case 3:
		std::cout << "settings page" << "\n";
		break;
	default:
		std::cout << "some another page" << "\n";
		break;
	}
	
	/*
	SWITCH целесообразно применять до 7 вариантов
	ну или на сколько позволяет лень
	больше 7ми вариантов лучше брать массив
	*/


	/*
	for-оператор цикла со счетом

	for(целоч.переменная ; условие продолжения ; повторяемое действие)
	{
	 тело цикла
	}
	
	*/

	std::cout << "\n";

	
	/*
	типичный случай: задают целую переменную счетчик int i
	задают максимальное значение
	задают возрастание/уменьшение i++
	*/
	
	for (int i = 0; i < 10; i++)
	{
		std::cout << "i= " << i << "\n";
	}
	std::cout << "\n";
	for (int i = 0, j = 5;
		i + j < 10;
		i++, j = j + 2, std::cout << "i = " << i << "\t" << "j = " << j << "\n")
	{
		//действия, повторяемые, пока i+j<10
	}

	for (int i = 0; i < 10; i++)
	{
		int l = 20;
		std::cout << "i=" << i << "\n";
		std::cout << "l=" << l << "\n";
		
	}









	

	getchar();
    return 0;
}

